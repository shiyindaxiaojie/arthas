# Changelog

Changelog of Arthas.

## 3.6.6 (2022-09-09)

### Features

- 新增 `arthas-tunnel-proxy` 代理，可视为 `arthas-tunnel-server` 的增强版。
- 新增 Arthas Agent 应用的自动服务发现，支持访问权限动态控制
- 新增 Basic 认证，避免用户直接访问控制台，造成生产事故。
